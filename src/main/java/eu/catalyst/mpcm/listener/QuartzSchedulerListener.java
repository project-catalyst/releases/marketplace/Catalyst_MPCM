package eu.catalyst.mpcm.listener;

import java.io.IOException;
import java.util.Properties;
import java.util.TimeZone;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import eu.catalyst.mpcm.utilities.Utilities;

public class QuartzSchedulerListener implements ServletContextListener {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(QuartzSchedulerListener.class);

	public void contextDestroyed(ServletContextEvent arg0) {
		//
	}

	public void contextInitialized(ServletContextEvent arg0) {

		TimeZone utcTimeZone = TimeZone.getTimeZone("UTC");
		TimeZone.setDefault(utcTimeZone);

		// execute Login operation to retrieve Token for next Rest Calls
		executeLogin();

		String MemoUrl = null;
		String proxyIp = null;
		String proxyPort = null;

		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		MemoUrl = System.getenv("MEMO_URL");
		log.debug("System.getenv(MEMO_URL): " + MemoUrl);
		if (MemoUrl == null) {
			MemoUrl = prop.getProperty("MEMOUrl");
			log.debug("prop.getProperty(MEMOUrl): " + MemoUrl);
		}
		Utilities.MemoUrl = MemoUrl;
		log.debug("Utilities.MemoUrl: " + Utilities.MemoUrl);

		proxyIp = System.getenv("PROXY_IP");
		log.debug("System.getenv(PROXY_IP): " + proxyIp);
		if (proxyIp == null) {
			proxyIp = prop.getProperty("proxyIp");
			log.debug("prop.getProperty(proxyIp): " + proxyIp);
		}
		Utilities.proxyIp = proxyIp;
		log.debug("Utilities.proxyIp: " + Utilities.proxyIp);

		proxyPort = System.getenv("PROXY_PORT");
		log.debug("System.getenv(PROXY_PORT): " + proxyPort);
		if (proxyPort == null) {
			proxyPort = prop.getProperty("proxyPort");
			log.debug("prop.getProperty(proxyPort): " + proxyPort);
		}
		Utilities.proxyPort = proxyPort;
		log.debug("Utilities.proxyPort: " + Utilities.proxyPort);

	}

	private void executeLogin() {
		log.debug("-> executeLogin");

		log.debug("<- executeLogin");
	}

}
