package eu.catalyst.mpcm.payload;
import java.io.Serializable;
public class ActionStatus implements Serializable {
    private static final long serialVersionUID = 1L;

	private Integer id;
	private String status;

	public ActionStatus() {
	}

	public ActionStatus(Integer id) {
		this.id = id;
	}

	public ActionStatus(Integer id, String status) {
		this.id = id;
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
