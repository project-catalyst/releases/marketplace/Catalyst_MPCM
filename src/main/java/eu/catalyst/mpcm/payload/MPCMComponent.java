package eu.catalyst.mpcm.payload;

import java.io.Serializable;

public class MPCMComponent implements Serializable {
	private static final long serialVersionUID = 1L;

	private MarketplaceComponentItem mpcm;

	public MPCMComponent() {
	}

	public MarketplaceComponentItem getMpcm() {
		return mpcm;
	}

	public void setMpcm(MarketplaceComponentItem mpcm) {
		this.mpcm = mpcm;
	}

}
