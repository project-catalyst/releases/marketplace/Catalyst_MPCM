package eu.catalyst.mpcm.payload;

import java.io.Serializable;

public class CorrelatedMarketActionsItem implements Serializable {
	private static final long serialVersionUID = 1L;

	private String marketplaceForm;
	private int actionId;

	public String getMarketplaceForm() {
		return marketplaceForm;
	}

	public void setMarketplaceForm(String marketplaceForm) {
		this.marketplaceForm = marketplaceForm;
	}

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
