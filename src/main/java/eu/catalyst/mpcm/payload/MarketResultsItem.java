package eu.catalyst.mpcm.payload;

import java.math.BigDecimal;
import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import eu.catalyst.mpcm.global.DateDeSerializer;
import eu.catalyst.mpcm.global.DateSerializer;

public class MarketResultsItem {

	private Integer id;

	@JsonDeserialize(using = DateDeSerializer.class)
	@JsonSerialize(using = DateSerializer.class)
	private Date actionStartTime;

	@JsonDeserialize(using = DateDeSerializer.class)
	@JsonSerialize(using = DateSerializer.class)
	private Date actionEndTime;

	private BigDecimal value;

	private String uom;

	private BigDecimal price;

	private String deliveryPoint;

	private String actionType;

	private String status;

	private BigDecimal clearingValue;

	private BigDecimal clearingPrice;

	public MarketResultsItem() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getActionStartTime() {
		return actionStartTime;
	}

	public void setActionStartTime(Date actionStartTime) {
		this.actionStartTime = actionStartTime;
	}

	public Date getActionEndTime() {
		return actionEndTime;
	}

	public void setActionEndTime(Date actionEndTime) {
		this.actionEndTime = actionEndTime;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getDeliveryPoint() {
		return deliveryPoint;
	}

	public void setDeliveryPoint(String deliveryPoint) {
		this.deliveryPoint = deliveryPoint;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getClearingValue() {
		return clearingValue;
	}

	public void setClearingValue(BigDecimal clearingValue) {
		this.clearingValue = clearingValue;
	}

	public BigDecimal getClearingPrice() {
		return clearingPrice;
	}

	public void setClearingPrice(BigDecimal clearingPrice) {
		this.clearingPrice = clearingPrice;
	}

}
