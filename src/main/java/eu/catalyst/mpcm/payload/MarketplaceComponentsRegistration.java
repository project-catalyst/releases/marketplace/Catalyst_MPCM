package eu.catalyst.mpcm.payload;
import java.io.Serializable;
public class MarketplaceComponentsRegistration implements Serializable {
    private static final long serialVersionUID = 1L;

	private String type;

	private String form;

	private MarketplaceComponents components;

	public MarketplaceComponentsRegistration() {
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public MarketplaceComponents getComponents() {
		return components;
	}

	public void setComponents(MarketplaceComponents components) {
		this.components = components;
	}

}
