package eu.catalyst.mpcm.payload;
import java.io.Serializable;
import java.util.List;

public class CorrelationRequestPUTDELETE implements Serializable {
    private static final long serialVersionUID = 1L;

	private String username;
	private Integer correlationId;
	private Integer constraintId;
	private String timeframe;
	private Long timestamp;
	private List<CorrelationActionOLD> actions;

	public String getTimeframe() {
		return timeframe;
	}

	public void setTimeframe(String timeframe) {
		this.timeframe = timeframe;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(Integer correlationId) {
		this.correlationId = correlationId;
	}

	public Integer getConstraintId() {
		return constraintId;
	}

	public void setConstraintId(Integer constraintId) {
		this.constraintId = constraintId;
	}

	public List<CorrelationActionOLD> getActions() {
		return actions;
	}

	public void setActions(List<CorrelationActionOLD> actions) {
		this.actions = actions;
	}

}
