package eu.catalyst.mpcm.payload;

import java.io.Serializable;
import java.util.Collection;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import com.fasterxml.jackson.annotation.JsonIgnore;


public class ActionType implements Serializable {
    private static final long serialVersionUID = 1L;
	private Integer id;
	private String type;

    public ActionType() {
    }

    public ActionType(Integer id) {
        this.id = id;
    }

    public ActionType(Integer id, String type) {
        this.id = id;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    
}
