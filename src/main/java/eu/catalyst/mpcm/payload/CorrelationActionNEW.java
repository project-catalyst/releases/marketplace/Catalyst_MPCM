package eu.catalyst.mpcm.payload;
import java.io.Serializable;
public class CorrelationActionNEW implements Serializable {
    private static final long serialVersionUID = 1L;

	private Integer informationBrokerId;
	private Integer sessionId;
	private Integer actionId;

	public Integer getInformationBrokerId() {
		return informationBrokerId;
	}

	public void setInformationBrokerId(Integer informationBrokerId) {
		this.informationBrokerId = informationBrokerId;
	}

	public Integer getSessionId() {
		return sessionId;
	}

	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}

	public Integer getActionId() {
		return actionId;
	}

	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}


}
