package eu.catalyst.mpcm.payload;
import java.io.Serializable;
import java.util.List;

public class CorrelationListResponseGET implements Serializable {
    private static final long serialVersionUID = 1L;

	private String username;
	private List<CorrelationResponseGET> correlations;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<CorrelationResponseGET> getCorrelations() {
		return correlations;
	}

	public void setCorrelations(List<CorrelationResponseGET> correlations) {
		this.correlations = correlations;
	}

}
