package eu.catalyst.mpcm.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import eu.catalyst.mpcm.payload.CorrelatedMarketActionsResponse;
import eu.catalyst.mpcm.payload.CorrelatedMarketActionsRequest;
import eu.catalyst.mpcm.utilities.Utilities;

@Stateless
@Path("/correlatedMarketActions/")
public class CorrelatedMarkeActions {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CorrelatedMarkeActions.class);

	public CorrelatedMarkeActions() {

	}

	@GET
	@Path("{marketActorName}/{timeframe}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })

	public List<CorrelatedMarketActionsResponse> getCorrelatedMarkeActions(
			@PathParam("marketActorName") String marketActorName, @PathParam("timeframe") String timeframe) {

		log.debug("-> getCorrelatedMarkeActions");
		log.debug("marketActorName:" + marketActorName);
		log.debug("timeframe:" + timeframe);

		List<CorrelatedMarketActionsResponse> myCorrelatedMarketActionsResponseList = null;

		try {

			URL url = new URL(
					Utilities.MemoUrl + "/correlatedMarketActions/" + marketActorName + "/" + timeframe + "/");

			log.debug("invokeGetMarketSession - Rest url: " + url.toString());
			
			HttpURLConnection connService;
			Proxy proxy;
			if (Utilities.proxyIp.equals("") || Utilities.proxyIp == null) {
				proxy = Proxy.NO_PROXY;
			} else {
				int proxyPort = Integer.parseInt(Utilities.proxyPort);
				proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Utilities.proxyIp, proxyPort));
			}
			connService = (HttpURLConnection) url.openConnection(proxy);
			
			connService.setDoOutput(true);
			connService.setRequestMethod("GET");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Content-Type", "application/json");

			if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
			Type listType = new TypeToken<List<CorrelatedMarketActionsResponse>>() {
			}.getType();
			GsonBuilder gb = new GsonBuilder();
			Gson gson = gb.create();
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			myCorrelatedMarketActionsResponseList = gson.fromJson(br, listType);
			log.debug("payload received: " + gson.toJson(myCorrelatedMarketActionsResponseList));
			br.close();
			connService.disconnect();
		} catch (

		Exception e) {
			e.printStackTrace();
		}

		log.debug("<- getCorrelatedMarkeActions");
		return myCorrelatedMarketActionsResponseList;
	}

	@POST
	@Path("{marketActorName}/{timeframe}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })

	public CorrelatedMarketActionsResponse postCorrelatedMarkeActions(
			@PathParam("marketActorName") String marketActorName, @PathParam("timeframe") String timeframe,
			CorrelatedMarketActionsRequest correlatedMarketActionsRequest) {

		log.debug("-> getCorrelatedMarkeActions");
		log.debug("marketActorName:" + marketActorName);
		log.debug("timeframe:" + timeframe);
		log.debug("correlatedMarketActionsRequest:" + new Gson().toJson(correlatedMarketActionsRequest));

		CorrelatedMarketActionsResponse myCorrelatedMarketActionsResponse = null;
		try {
			URL url = new URL(
					Utilities.MemoUrl + "/correlatedMarketActions/" + marketActorName + "/" + timeframe + "/");

			log.debug("invokeGetMarketActionByMarketPlaceSessionId - Rest url: " + url.toString());
			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
			connService.setDoOutput(true);
			connService.setRequestMethod("POST");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Content-Type", "application/json");

			GsonBuilder gb = new GsonBuilder();
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			Gson gson = gb.create();
			String record = gson.toJson(correlatedMarketActionsRequest);
			log.debug("Rest Request: " + record);

			OutputStream os = connService.getOutputStream();
			os.write(record.getBytes());

			int myResponseCode = connService.getResponseCode();
			log.debug("HTTP error code :" + myResponseCode);

			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
			Type listType = new TypeToken<CorrelatedMarketActionsResponse>() {
			}.getType();
			gb = new GsonBuilder();
			gson = gb.create();
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			myCorrelatedMarketActionsResponse = gson.fromJson(br, listType);
			log.debug("payload received: " + gson.toJson(myCorrelatedMarketActionsResponse));
			br.close();
			connService.disconnect();
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		log.debug("<- postCorrelatedMarkeActions");
		return myCorrelatedMarketActionsResponse;
	}

}
