package eu.catalyst.mpcm.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.List;
import com.google.gson.reflect.TypeToken;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.catalyst.mpcm.payload.ReferencePriceItem;
import eu.catalyst.mpcm.utilities.Utilities;

@Stateless
@Path("/referencePricesPreviousDay/")
public class ReferencePrices {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReferencePrices.class);

	public ReferencePrices() {

	}

	@GET
	@Path("{marketActorName}/{marketplaceForm}/{timeframe}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })

	public List<ReferencePriceItem> getReferencePricesPreviousDay(@PathParam("marketActorName") String marketActorName,
			@PathParam("marketplaceForm") String marketplaceForm, @PathParam("timeframe") String timeframe) {

		log.debug("-> getReferencePricesPreviousDay");
		log.debug("marketActorName:" + marketActorName);
		log.debug("marketplaceForm:" + marketplaceForm);
		log.debug("timeframe:" + timeframe);

		List<ReferencePriceItem> myReferencePriceList = null;

		try {
			URL url = new URL(Utilities.MemoUrl + "/referencePricesPreviousDay/" + marketActorName + "/"
					+ marketplaceForm + "/" + timeframe + "/");

			log.debug("invokeGetMarketSession - Rest url: " + url.toString());
			
			HttpURLConnection connService;
			Proxy proxy;
			if (Utilities.proxyIp.equals("") || Utilities.proxyIp == null) {
				proxy = Proxy.NO_PROXY;
			} else {
				int proxyPort = Integer.parseInt(Utilities.proxyPort);
				proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Utilities.proxyIp, proxyPort));
			}
			connService = (HttpURLConnection) url.openConnection(proxy);
			
			connService.setDoOutput(true);
			connService.setRequestMethod("GET");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Content-Type", "application/json");

			if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
			Type listType = new TypeToken<List<ReferencePriceItem>>() {
			}.getType();
			GsonBuilder gb = new GsonBuilder();
			Gson gson = gb.create();
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			myReferencePriceList = gson.fromJson(br, listType);
			log.debug("payload received: " + gson.toJson(myReferencePriceList));
			br.close();
			connService.disconnect();

		} catch (

		Exception e) {
			e.printStackTrace();
		}
		// return the array complete
		log.debug("<- getReferencePricesPreviousDay");
		return myReferencePriceList;
	}

}
