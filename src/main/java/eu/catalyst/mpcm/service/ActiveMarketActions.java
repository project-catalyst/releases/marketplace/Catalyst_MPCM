package eu.catalyst.mpcm.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import eu.catalyst.mpcm.payload.ActiveMarketActionItemGET;
import eu.catalyst.mpcm.payload.ActiveMarketActionItemRequestPOST;
import eu.catalyst.mpcm.payload.ActiveMarketActionItemResponsePOST;
import eu.catalyst.mpcm.utilities.Utilities;

@Stateless
@Path("/marketActions/")
public class ActiveMarketActions {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ActiveMarketActions.class);

	public ActiveMarketActions() {

	}

	@GET
	@Path("{marketActorName}/{marketplaceForm}/{timeframe}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })

	public List<ActiveMarketActionItemGET> getActiveMarketActions(@PathParam("marketActorName") String marketActorName,
			@PathParam("marketplaceForm") String marketplaceForm, @PathParam("timeframe") String timeframe) {

		log.debug("-> getActiveMarketActions");
		log.debug("marketActorName:" + marketActorName);
		log.debug("marketplaceForm:" + marketplaceForm);
		log.debug("timeframe:" + timeframe);

		List<ActiveMarketActionItemGET> myActiveMarketActionItemGETList = null;
		try {

			URL url = new URL(Utilities.MemoUrl + "/marketActions/" + marketActorName + "/" + marketplaceForm + "/"
					+ timeframe + "/");

			log.debug("invokeGetMarketSession - Rest url: " + url.toString());

			HttpURLConnection connService;
			Proxy proxy;
			if (Utilities.proxyIp.equals("") || Utilities.proxyIp == null) {
				proxy = Proxy.NO_PROXY;
			} else {
				int proxyPort = Integer.parseInt(Utilities.proxyPort);
				proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Utilities.proxyIp, proxyPort));
			}
			connService = (HttpURLConnection) url.openConnection(proxy);
			
			connService.setDoOutput(true);
			connService.setRequestMethod("GET");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Content-Type", "application/json");

			if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
			Type listType = new TypeToken<List<ActiveMarketActionItemGET>>() {
			}.getType();
			GsonBuilder gb = new GsonBuilder();
			Gson gson = gb.create();
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			myActiveMarketActionItemGETList = gson.fromJson(br, listType);
			log.debug("payload received: " + gson.toJson(myActiveMarketActionItemGETList));
			br.close();
			connService.disconnect();
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		log.debug("<- getActiveMarketActions");
		return myActiveMarketActionItemGETList;
	}

	@POST
	@Path("{marketActorName}/{marketplaceForm}/{timeframe}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })

	public List<ActiveMarketActionItemResponsePOST> postActiveMarketActions(
			@PathParam("marketActorName") String marketActorName, @PathParam("marketplaceForm") String marketplaceForm,
			@PathParam("timeframe") String timeframe,
			List<ActiveMarketActionItemRequestPOST> myActiveMarketActionItemRequestPOSTList) {

		log.debug("-> postActiveMarketActions");
		log.debug("marketActorName:" + marketActorName);
		log.debug("marketplaceForm:" + marketplaceForm);
		log.debug("timeframe:" + timeframe);
		log.debug("myActiveMarketActionItemRequestPOST:" + new Gson().toJson(myActiveMarketActionItemRequestPOSTList));

		List<ActiveMarketActionItemResponsePOST> myActiveMarketActionItemResponsePOSTList = null;
		try {

			URL url = new URL(Utilities.MemoUrl + "/marketActions/" + marketActorName + "/" + marketplaceForm + "/"
					+ timeframe + "/");

			log.debug("Rest url: " + url.toString());
			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
			connService.setDoOutput(true);
			connService.setRequestMethod("POST");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Content-Type", "application/json");

			GsonBuilder gb = new GsonBuilder();
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			Gson gson = gb.create();
			String record = gson.toJson(myActiveMarketActionItemRequestPOSTList);
			log.debug("Rest Request: " + record);

			OutputStream os = connService.getOutputStream();
			os.write(record.getBytes());

			int myResponseCode = connService.getResponseCode();
			log.debug("HTTP error code :" + myResponseCode);

			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
			Type listType = new TypeToken<List<ActiveMarketActionItemResponsePOST>>() {
			}.getType();
			gb = new GsonBuilder();
			gson = gb.create();
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			myActiveMarketActionItemResponsePOSTList = gson.fromJson(br, listType);
			log.debug("payload received: " + gson.toJson(myActiveMarketActionItemResponsePOSTList));
			br.close();
			connService.disconnect();
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		log.debug("<- postActiveMarketActions");
		return myActiveMarketActionItemResponsePOSTList;
	}

}
